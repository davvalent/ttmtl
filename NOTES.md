# Notes techniques

## Miroir

```bash
$ git push --mirror git@github.com:davvalent/ttmtl-corpus-mirror.git
```

# Notes de travail

## Objectifs généraux

Explorer les documents en vue de la constitution du corpus :

- explorer les documents pressentis du corpus
- apprendre à connaître les documents, les collections, les institutions, les éditeurs, tout.

Sélection des documents par échantillonage raisonné (théorique?).

**La première phase est exploratoire** : elle vise à identifier des documents historiques et des entités géographiques d'intérêt, à développer une connaissance générale du corpus présenti.

## Objectifs spécifiques

- dresser la liste des fonds (à MTL, Ottawa, Québec, musées, universités); travail en cours sur Zotero, _Phd/corpus
- consigner des documents et structurer des métadonnées
- préparer une publication qui traitera de mon corpus (sous forme de plusieurs courts billets)

Autres :

- il serait très intéressant de réfléchir, éventuellement, au géoréférencement des documents cartographiques

## Outils

- annotation? **recogito**?
- les documents pressentis avec **Tropy** : `/opt/tropy/tropy`
- premier entrepôt RDF (**Jena**) avec Abadie
- voir aussi, Trudel (important)

## Métho SCI7010

- quelles sont les entités géographiques qui apparaissent autour de l'entité A entre l'année N et l'année N+10?
- quelles sont les relations topologiques? montre moi les inférences.
- sur quels documents?
- sur quels documents appairait l'entité B?
- dresse la chronologie des relations topologiques de cette entité selon la cartographie
- quelles entités apparaissent sur quelles cartes, publiées en quelle année?

## Communications

- Modéliser la chronologie de la topologie cartographique de Montréal
- Ontologie pour la chronologie topologique de la cartographie historique de Montréal
- présentation et description du corpus

# Tâches

## Références

Établir une liste de références bibliographiques utiles à la réflexion sur le corpus. À faire avec Zotrero. Voir tag `corpus`, préciser sa nature : pour sources primaires ou autre?

- aspects généraux : littérature sur la cartographie historique
- aspects spcifiques : littérature sur la cartographie historiques de Montréal

## Corpus

Rechercher dans les collections numériques.

Critères de recherche larges dans un premier temps :

- 1840-1980

## Métadonnées

Structurer les métadonnées du corpus à l'aide de Tropy. Sélectionner un modèle de départ.

Quelle structure?

- Description archivistique avancée (RiCO, Abadie)?
- Description générique (Dubli Core)?
- Les deux?
- CIDOC?

Note : Tropy permet d'annoter les documents.

## Géoréférencement

Concevoir un protocole de géoréférencement : établir dans un premier temps le plus précisément possible la couverture spatiale avec le couple de coordonnées permettant d'établir les limites rectangualires de la couverture (boundings). Devra probablement passer par Qgis. Peut-être par [https://www.ohmg.dev/](https://www.ohmg.dev/). Voir aussi pour manifeste **IIIF**.

J’aimerais que le plus grand nombre possible  de documents de ce corpus soit géoréférencé (et tuilé).
Il faudra probablement demeurer modeste, voire pour ce faire travailler sur un échantillon.
Comme la taille du corpus à l'étude est à ce jour inconnue, la quantification de cet objectif devra être précisée ultérieurement.
Voir l'étiquette https://www.zotero.org/groups/4948254/sci7001_-_david_valentine/tags/georeferencing.

GIS infrastruture with xyz tiles or IIIF Georeference Extension? 
Both?
Quelles options pour quelles fins?

https://iiif.io/api/extension/georef/ :

> A solution is to include this information in the metadata of the IIIF Manifest, or in a machine readable format referenced through the seeAlso property.

Tropy permet de travailler avec IIIF. À explorer.

## Publication

Voir projet MTL Triple Store.
