# ttmtl-corpus

Dépôt de documents pour la phase exploratoire des travaux de constitution d’un corpus de cartes historiques montréalaises.

Ces travaux sont effectués dans le cadre du projet de recherche doctoral intitulé *Les transformations territoriales de Montréal sous la loupe des graphes de connaissances* (*working title*). La thèse est préparée par David Valentine à l’école de bibliothéconomie et des sciences de l’information (EBSI) de l’Université de Montréal. Ce projet de recherche reçoit l’appui financier du Conseil de recherche en sciences humaines du Canada (CRSH) dans le cadre du programme de Bourses d’études supérieures du Canada au niveau du doctorat (BESC D).

Un miroir de ce dépôt est disponible sur GitHub : https://github.com/davvalent/ttmtl-corpus-mirror
