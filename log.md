# Journal de terrain

Structure d'une séance de travail :

- objectifs
- remarques
- difficultés
- suite

# 2024-02-06

- durée : 
- emplacement : bureau@UdeM

## Objectif

Reprise en main de Tropy avec notes nécessaires pour entrer directement dans le vif du sujet la prochaine fois.

## Remarques

- révisions des notes laissées lors de la dernière séance
- ajout d'un document dans Tropy

## Difficultés

- beaucoup de temps perdu à relire les notes, mais pas inutile
- un seul document d'ajouter, plusieurs questions en supens quant à la façon de structurer les métadonnées (choix d'un modèle de métadonnées, typologie des documents, etc.)
- les métadonnées des documents en ligne sont peu claires, beaucoup d'ambiguité

## Suite

- reprendre là où je laisse ça aujourd'hui pour l'exploration dans Tropy
- revoir la suite indiquée le 2024-02-05, et surtout réfléchir aux métadonnées utiles pour ce projet, mais utile en général pour les documents

# 2024-02-05

- durée : 1h
- emplacement : bureau@perso

## Objectif

Réfléchir aux tâches à réaliser dans le cadre du projet d'exploration pour la constitution d'un corpus.

## Remarques

Un premier jet des tâches à mener a été consigné dans [NOTES.md](./NOTES.md). Les tâches [corpus](./NOTES.md#corpus) et [métadonnées](./NOTES.md#métadonnées) me semblent prioritaires pour l'instant.

## Difficultés

L'objectif est globalement atteint de façon satisfaisante.

## Suite

Repérer les documents actuellement connus (utilisés dans des présentations ou mentionnés quelque part dans le projet de thèse) et les référencer dans Tropy avec un prototype de structure pour les métadonnées. Essayer d'en savoir plus sur IIIF et sur le lien entre IIIF et les métadonnées structurées.